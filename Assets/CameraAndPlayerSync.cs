using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAndPlayerSync : MonoBehaviour
{
    [SerializeField] private GameObject scoreCard;
   float aspect;
   float worldHeight;
   float worldWidth;

   public float MIN_X;
   public float MAX_X;
   public float MAX_Y;
   public float MIN_Y;
   
   private void Start()
   {
       //float height = 2f * cam. orthographicSize;
      
       aspect = (float)Screen.height / Screen.width;

       Debug.Log("aspect ration is "+aspect);
       worldHeight = GetComponent<Camera>().orthographicSize * 2;
       float width = worldHeight * GetComponent<Camera>(). aspect;
       Debug.Log("world height ration is "+worldHeight);
       worldWidth = worldHeight * aspect;
       Debug.Log("world width ration is "+worldWidth);

       //Setting the alignment of Score with Camera
       var scorePos = scoreCard.GetComponent<RectTransform>();
       scorePos.sizeDelta = new Vector2(width, worldHeight);



   }
   
    void Update()
    {
        MAX_X = worldWidth;
        MAX_Y = worldHeight;
        MIN_X = worldWidth * -1;
        MIN_Y = worldHeight * -1;

        RestrictCamera();
    }


    void RestrictCamera()
    {
        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, MIN_X, MAX_X),
            Mathf.Clamp(transform.position.y, MIN_Y, MAX_Y),
            -10);
        
        

    }
    
}
