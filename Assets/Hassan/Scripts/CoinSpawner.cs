using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
    [SerializeField] private CoinsDisplay _coinsDisplay;

    [SerializeField] private GameObject coin;

    [SerializeField] private TextMeshPro score;



    public static CoinSpawner instance;
    // Start is called before the first frame update
    void Start()
    {
        SpawnCoins();
       
    }

    public void updateScore(string name,string scorePoints) {
        
        score.text =name+" "+scorePoints;
    }
    private void Awake() {
        
        if (instance != null) {
            Destroy(gameObject);
        }
        else{
            instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
       
    }


    void SpawnCoins() {

        for (int i = 0; i < _coinsDisplay.contents.Count; i++) {
            
            Instantiate(coin,new Vector3(_coinsDisplay.contents[i].xPos, _coinsDisplay.contents[i].yPos, 0),quaternion.identity);
        }
    }

    private void OnDestroy() {
        //scoreCount++;
       
    }
    
    
    
}
