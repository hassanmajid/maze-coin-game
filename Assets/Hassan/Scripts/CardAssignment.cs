using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class CardAssignment : MonoBehaviour
{

    [SerializeField] private Text text;

    private string input,number;
    public static  Action<string,int> onCardSelected;
   
  
    //private PhotonView photonView;
   
    private int level=0;
    private PhotonView _photonView;
    private void Start() {
      //  _photonView=gameObject.GetComponent<PhotonView>();
     // CardManager.textAssign += AssignText;
    }

    private void OnDestroy() {
        //CardManager.textAssign -= AssignText;
    }

    public void AssignValues(int value,PhotonView view) {
        level = value;
       // _photonView=gameObject.GetComponent<PhotonView>();
        _photonView = view;
       // text.text = input;
   }

   public void AssignText(int value) {

      
         
          text.text =value.ToString();
      

   }
    

    public void onClick() {
        
       
        onCardSelected.Invoke(text.text,level);
        //AssignText(text.text);
    }
    
}
