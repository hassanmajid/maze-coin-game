using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class CreateAndJoinRooms : MonoBehaviourPunCallbacks
{
    [SerializeField] private InputField joinRoom;
    [SerializeField] private InputField createRoom;

    [SerializeField] private GameObject playButton;
    [SerializeField] private GameObject playerName;
    [SerializeField] private GameObject joinRoomObj;
    [SerializeField] private GameObject CreateRoomObj;

    [SerializeField] private GameObject playerAge;
   // private Text playersCount;
    [SerializeField] private Text playersName;

    [SerializeField] GameObject
    PlayerSpawnText;

    [SerializeField] private RectTransform playerPanel;
    private List<Player> playerList;


    public void setPlayerName() {

        PhotonNetwork.LocalPlayer.NickName = playersName.text;
        
        playerName.SetActive(false);
        //playerAge.SetActive(false);
        joinRoomObj.SetActive(true);
        CreateRoomObj.SetActive(true);
        
    }
    public void CreateRoom() {
        PhotonNetwork.CreateRoom(createRoom.text);
        playButton.SetActive(true);
    }

    public void JoinRoom() {
        
        PhotonNetwork.JoinRoom(joinRoom.text);
    }

    void setPlayersList(List<Player> player) {
       

        if (PhotonNetwork.PlayerList.Length > 0) {
            Debug.Log("totalPlayers= "+PhotonNetwork.PlayerList.Length);
            for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++) {

                if (!player.Contains(PhotonNetwork.PlayerList[i])) {
                    var name = Instantiate(PlayerSpawnText, playerPanel);
                    var Username = name.GetComponent<Text>();
                    Username.text = PhotonNetwork.PlayerList[i].NickName;
                    player.Add(PhotonNetwork.PlayerList[i]);
                }
                
            }
            
        }
      
        
    }

    void SpawnPlayers(List<Player> playerList) {
        
        for (int i = 0; i < playerList.Count; i++) {

            var name = Instantiate(playerName, playerPanel);
            var Username = name.GetComponent<Text>();
            Username.text = playerList[i].NickName;

        }
    }
    public override void OnJoinedRoom() {
        
        
        setPlayersList(playerList);
        
    }
    
    public override void OnPlayerEnteredRoom(Player newPlayer) {
       
     
       setPlayersList(playerList);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer) {
        base.OnPlayerLeftRoom(otherPlayer);
        setPlayersList(playerList);
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps) {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        setPlayersList(playerList);
    }

    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged) {
        foreach (var item in propertiesThatChanged) {
            Debug.Log("key= "+item.Key+" value= "+item.Value);

            var key = (byte) item.Key;
            var roomClose = (bool) item.Value;
            
            if (key == GamePropertyKey.IsOpen) {
                if (!roomClose) {
                    PhotonNetwork.LoadLevel("Game");
                } 
            }
            
            
        }
    }

    public void play() {
        
        PhotonNetwork.CurrentRoom.IsOpen = false;

    }
    // Start is called before the first frame update
    void Start()
    {
        playButton.SetActive(false);
        playerAge.SetActive(false);
        playerList = new List<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
