using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class PlayerMovement : MonoBehaviourPun
{
    public float speed;
    private Rigidbody2D _rigidbody2D;

  
    public int scoreCount;
    private GameObject _camera;
   // private PhotonView _photonView;
    

  
    
    private void Start() {
        var parentObj = GameObject.FindWithTag("panel");


        transform.position = new Vector3(parentObj.transform.position.x, parentObj.transform.position.y,
            parentObj.transform.position.z);
        //transform.position = GameManager.position;
        
        transform.SetParent(parentObj.transform);
        
        _camera = GameObject.FindWithTag("MainCamera");
        
        _rigidbody2D = GetComponent<Rigidbody2D>();
        CoinSpawner.instance.updateScore(PhotonNetwork.LocalPlayer.NickName,scoreCount.ToString());
    }

    void Update () {

        if (photonView.IsMine) {


            _camera.transform.position = new Vector3(transform.position.x, transform.position.y, -10);
            if (Input.touchCount > 0) {
                var touch = Input.GetTouch(0);
                if (touch.position.x < Screen.width / 2) {
                    transform.position += Time.deltaTime * speed * Vector3.left;
                }
                else if (touch.position.x > Screen.width / 2) {
                    transform.position += Time.deltaTime * speed * Vector3.right;
                }
                else if (touch.position.y > Screen.height / 2) {
                    transform.position += Time.deltaTime * speed * Vector3.down;
                }
                else if (touch.position.y < Screen.height / 2) {
                    transform.position += Time.deltaTime * speed * Vector3.up;

                }

            }


            if (Input.GetKey(KeyCode.RightArrow)) {
                transform.position += Time.deltaTime * speed * Vector3.right;
            }

            if (Input.GetKey(KeyCode.LeftArrow)) {


                transform.position += Time.deltaTime * speed * Vector3.left;
            }

            if (Input.GetKey(KeyCode.UpArrow)) {


                transform.position += Time.deltaTime * speed * Vector3.up;
            }

            if (Input.GetKey(KeyCode.DownArrow)) {

                transform.position += Time.deltaTime * speed * Vector3.down;
            }
        }

    }

    
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Coins")) {
            
            if (photonView.IsMine) {
                scoreCount++;
                CoinSpawner.instance.updateScore(PhotonNetwork.LocalPlayer.NickName,scoreCount.ToString());
               
            }
            Destroy(other.gameObject);
           
        }
        
    }
}
