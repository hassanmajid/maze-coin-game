using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using UnityEngine;
using UnityEngine.UI;

public class CardManager : MonoBehaviourPun
{
    [SerializeField] private CardAssignment card;
    public static  Action<int> textAssign;
    private bool toggle,toggle2;
     private Text cardDescription;
    private string input;
   // private Text text;
    private Image image;
    private static int levelValue;
    GameObject parent;
    private void Start() {
        image = card.GetComponent<Image>();
      
       setPosition();
       
       if (photonView.IsMine) {
           CardAssignment.onCardSelected += onCardSelectParent;
       }
       
       

    }

    private void OnDestroy() {
        
        if (photonView.IsMine) {
            CardAssignment.onCardSelected -= onCardSelectParent;
        }
    }

    public void setPosition() {

        parent = GameObject.FindWithTag("panel1");
  
        transform.position = GameManager.position;
        
        transform.SetParent(parent.transform);
        
        if (photonView.IsMine) {
            for (int i = 0; i < 3; i++) {
            
                var obj = Instantiate(card, transform.parent);
                obj.AssignValues(++levelValue, photonView);
                
                obj.AssignText(levelValue);
            }
        }
        else {
            
        }
        
        
    }

    [PunRPC]
    
    public void SetText(String text,int level) {
      
        if (photonView.IsMine) {
        
            if (!toggle) {
                // image.color=Color.blue;
                text =PhotonNetwork.LocalPlayer.NickName+" Card ON "+ (level);
                //SetText("Mine Card");
                toggle = true;
            }
            else {
                //image.color=Color.white;
                text =PhotonNetwork.LocalPlayer.NickName+" Card OFF "+ (level);
                toggle = false;
                
            }
            
          
            input = text;
            setCardDescription(text);
            Debug.Log( text);
           
        }
        
         else {
             
             if (!toggle2) {
                 //  image.color=Color.red;
                 text = PhotonNetwork.PlayerListOthers[0].NickName+" Card On "+(level);
                 toggle2 = true;
             }
             else {
                 //image.color=Color.white;
                 text = PhotonNetwork.PlayerListOthers[0].NickName+" Card OFF "+(level);
                 toggle2 = false;
             }
             
             input = text;
             setCardDescription(text);
         }

        
    }

    void setCardDescription(string text) {
        
        var TextDesc=GameObject.FindWithTag("Carddescription");
      //  transform.SetParent(TextDesc.transform);
        //cardDescription.transform.SetParent(TextDesc.transform);
        var txtD = TextDesc.GetComponent<Text>();
        txtD.text = text;
       // cardDescription.text = text;
    }
    
    public void  onCardSelectParent(string text,int level) {
        
        //card.AssignText(input);
        photonView.RPC("SetText", RpcTarget.All, text,level);
        
        
    }

    


    

}
