using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    [CreateAssetMenu(fileName = "CoinDisplay")]
    public class CoinsDisplay : ScriptableObject
    {
        [SerializeField] public List<CoinContentTypeItem> contents;
    }


[System.Serializable]
    public class CoinContentTypeItem
    {
        [SerializeField] public float xPos;

        [SerializeField] public float yPos;

        
    }

